variable "profile" {
  description = "The AWS profile to use"
  default = "default"
}

variable "instance_type" {
  description = "The AWS instance type to create"
  default = "t3a.nano"
}

variable "region" {
  description = "The AWS region"
  default = "us-east-1"
}

variable "ami" {
  description = "The Amazon Machine Image ID"
  # no default
}

variable "key_name" {
  description = "The SSH key name"
  # no default
}

variable "cidr_block_vpc" {
  default = "172.17.2.0/24"
}

provider "aws" {
  profile = var.profile
  region = var.region
}


resource "aws_vpc" "openvpn_vpc" {
  cidr_block = var.cidr_block_vpc
  tags = {
    Name = "openvpn_vpc"
  }
}

resource "aws_internet_gateway" "openvpn_internet_gateway" {
  vpc_id = aws_vpc.openvpn_vpc.id
  tags = {
    Name = "openvpn_internet_gateway"
  }
}

resource "aws_subnet" "openvpn_subnet" {
  cidr_block = var.cidr_block_vpc
  vpc_id = aws_vpc.openvpn_vpc.id
  tags = {
    Name = "openvpn_subnet"
  }
}

resource "aws_security_group" "openvpn_security_group" {
  vpc_id = aws_vpc.openvpn_vpc.id
  tags = {
    Name = "openvpn_security_group"
  }
  ingress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    self = true
    description = "Allow inbound traffic within security group"
  }
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow inbound SSH traffic"
  }
  ingress {
    from_port = 1194
    to_port = 1194
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow inbound OpenVPN traffic"
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow all outbound traffic"
  }
}

resource "aws_route_table" "openvpn_route_table" {
  vpc_id = aws_vpc.openvpn_vpc.id
  tags = {
    Name = "openvpn_route_table"
  }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.openvpn_internet_gateway.id
  }
}

resource "aws_route_table_association" "openvpn_route_table_association" {
  subnet_id = aws_subnet.openvpn_subnet.id
  route_table_id = aws_route_table.openvpn_route_table.id
}

resource "aws_instance" "openvpn" {
  ami = var.ami
  instance_type = var.instance_type
  key_name = var.key_name
  vpc_security_group_ids = ["${aws_security_group.openvpn_security_group.id}"]
  subnet_id = aws_subnet.openvpn_subnet.id
  associate_public_ip_address = true
  source_dest_check = false
  tags = {
    Name = "OpenVPN"
  }
}
