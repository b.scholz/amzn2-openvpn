# How to set up an openvpn server on Amazon Linux 2

Adapted for Amazon Linux 2 from https://www.digitalocean.com/community/tutorials/how-to-set-up-an-openvpn-server-on-ubuntu-18-04

## Build an Amazon Machine Image (AMI) with packer

Use [packer](https://www.packer.io/) to create an AMI in the us-east-1 region:

    cd packer
    packer build amzn2-openvpn.json

To build an AMI in a different region:

    packer build -var 'region=us-west-1' amzn2-openvpn.json

Note that the AMI must be created in the region where you plan to run the VPN.

This will create an AMI with:

 * Amazon Linux 2
 * The Extra Packages for Enterprise Linux (EPEL) repository enabled
 * openvpn, easy-rsa, iptables-services installed
 * The `make_config.sh` script from DigitalOcean to create openvpn configuration files

## Deploy an instance with terraform

Grab the AMI ID that was output by packer (or from the Amazon EC2 console).

    cd terraform
    terraform init
    terraform apply

If you built your AMI in a region other than `us-east-1`, specify the region on the command line:

    terraform apply -var 'region=us-west-1'

This will prompt you for the AMI ID and the name of the ssh key file to use,
and then create an t3a.nano instance.

## Configure your server

These steps assume you have a different machine that you will use as your
Certificate Authority (CA) server, and that you can securely copy files
to and from your OpenVPN server from your CA server using `scp`.

### Create a request to sign

On your OpenVPN server:

    cd ~/easy-rsa
    ./easyrsa init-pki
    ./easyrsa gen-req server nopass
    sudo cp pki/private/server.key /etc/openvpn/

### Sign the request

On your CA server, fetch the request from your OpenVPN server using `scp`:

    scp -i ~/<server.pem> ec2-user@<server>:easy-rsa/pki/reqs/server.req /tmp

Import and sign the request:

    ./easyrsa import-req /tmp/server.req server
    ./easyrsa sign-req server server

Put the signed certificate and the root certificate on the openvpn server:

    scp -i ~/.ssh/<server.pem> pki/issued/server.crt ec2-user@<server>:/tmp
    scp -i ~/.ssh/<server.pem> pki/ca.crt ec2-user@<server>:/tmp

### Add keys and certs to OpenVPN

Set up certs and keys on your OpenVPN server:

    sudo cp /tmp/{server.crt,ca.crt} /etc/openvpn
    cd ~/easy-rsa
    ./easyrsa gen-dh
    openvpn --genkey --secret ta.key
    sudo cp pki/dh.pem /etc/openvpn
    sudo cp ta.key /etc/openvpn

Copy key and cert for use by the make_config.sh script:

    cp ~/easy-rsa/ta.key ~/client-configs/keys
    sudo cp /etc/openvpn/ca.crt ~/client-configs/keys
    sudo chown ec2-user:ec2-user ~/client-configs/keys/ca.crt

### Restart OpenVPN

    sudo systemctl restart openvpn@server

## Add a client

### Generate a client certificate request:

On your OpenVPN server:

    cd ~/easy-rsa
    ./easyrsa gen-req client1 nopass
    cp pki/private/client1.key ~/client-configs/keys/

### Sign the certificate

On your CA server:

    scp -i ~/.ssh/<server.pem> ec2-user@<server>:easy-rsa/pki/reqs/client1.req /tmp
    ./easyrsa import-req /tmp/client1.req client1
    ./easyrsa sign-req client client1
    scp -i ~/.ssh/<server.pem> pki/issued/client1.crt ec2-user@<server>:/tmp

### Create an openvpn configuration file for the client

On your OpenVPN server:

    cd ~/client-configs
    cp /tmp/client1.crt keys/
    ./make_config.sh client1

This should create a file named `~/client-configs/files/client1.ovpn`.
Transfer this file to your client, and add a `hosts` file entry for
`amzn2-openvpn-server` that points to the IP address of your OpenVPN server.
Then connect to your OpenVPN server using:

    sudo openvpn --config client1.ovpn

### Test your VPN

Try dnsleaktest.com - it should show your traffic originating from your VPN server, not your local ISP.


## Certificate Authority (CA) server setup (on Debian Linux)

Set up a CA server using easy-rsa:

    supo apt-get install easy-rsa
    cp -r /usr/share/easy-rsa ~/
    cd ~/easy-rsa
    cp vars.example vars

Edit the `vars` file to provide your info:

    set_var EASYRSA_REQ_COUNTRY     "MyCountry"
    set_var EASYRSA_REQ_PROVINCE    "MyState"
    set_var EASYRSA_REQ_CITY        "MyCity"
    set_var EASYRSA_REQ_ORG         "MyCompany"
    set_var EASYRSA_REQ_OU          "MyDepartment"
    set_var EASYRSA_REQ_EMAIL       "first.last@example.com"

Initialize your CA:

    ./easyrsa init-pki
    ./easyrsa build-ca nopass
